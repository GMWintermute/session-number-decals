 
# Generic Session Numbers Decal Extension by GM Wintermute #  

This extension provides desktop decals for Fantasy Ground that display a Game Session number.  This extension is setup to handle 50 game sessions.  

### Required Software ###

Fantasy Grounds (http://www.fantasygrounds.com)

### How do I get set up? ###

Zip up all the contents of the custom decal folder but NOT the folder itself. Rename the zip file as filename.ext and copy it to your %appdata%\Fantasy Grounds\Extensions folder 

### Special Thanks ###

Special thanks to Mask of Winter for his tutorial on how to make custom decal extensions.  Check out the tutorial on the FG-CON website at http://www.fg-con.com/wp-content/uploads/2015/05/Mask_Decal_Class.pdf 
